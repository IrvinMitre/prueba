import { Router } from 'express'
import MutationController from '../controllers/mutation/mutation.controller'
import authMiddleware from '../jwt/authMiddleware'


export class MutationRouter {
    mutation = new MutationController()
    router: Router
    constructor(){
        this.router = Router();
        this.init();
    }

    init = () => {
        this.router.post('/',[authMiddleware], this.mutation.verifyAdn);
        this.router.get('/',[authMiddleware],  this.mutation.Getstatistics);
    }



}
const MutationRoutes = new MutationRouter();
MutationRoutes.init();
export default (MutationRoutes.router);



