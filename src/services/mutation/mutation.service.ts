import { AdnInterface } from "../../models/dna.interface";
import dnaRegister from "../../models/dna.model";

class MutationService {
  dna: any = new dnaRegister();
  private validaMutacion = /A{4}|C{4}|G{4}|T{4}/gi;
  constructor() {}

  async createDna(dna: AdnInterface) {
    const dna_new = new dnaRegister(dna);
    return await dna_new.save();
  }

  async GetDna(dna: AdnInterface) {
    return await dnaRegister.find({ dna: dna });
  }

  async Getstatistics() {
    return await dnaRegister.find({});
  }

  dnaValidation(dna: string[]) {
    if (!Array.isArray(dna)) {
      return 1;
    }
    if (dna.length === 0) {
      return 2;
    }
    const dnaStr = dna.join("");
    if (dna.length !== dnaStr.length / dna.length) {
      return 3;
    }
    if (!dnaStr.match(/^[ACGT]+$/)) {
      return 4;
    }
    return 5;
  }

  hasMutation(dna: string[]) {
    let mutant = 0;

    mutant += this.hasHorizontalSequence(dna);
    if (mutant > 0) {
      return true;
    }

    mutant += this.hasVerticalSequence(dna);
    if (mutant > 1) {
      return true;
    }

    mutant += this.hasDiagonalSequence(dna, false);
    if (mutant > 1) {
      return true;
    }

    mutant += this.hasDiagonalSequence(dna, true);
    if (mutant > 1) {
      return true;
    }

    return false;
  }

  private hasHorizontalSequence(dna: string[]): number {
    let count = 0;
    for (let i = 0; i < dna.length; i++) {
      if (dna[i].match(this.validaMutacion)) {
        ++count;
      }
    }
    return count;
  }

  private hasVerticalSequence(dna: string[]): number {
    let count = 0;
    for (let i = 0; i < dna.length; i++) {
      let cadena = "";
      for (let j = 0; j < dna.length; j++) {
        cadena += dna[j][i];
      }
      if (cadena.match(this.validaMutacion)) {
        ++count;
      }
    }
    return count;
  }

  private hasDiagonalSequence(
    dna: string[],
    bottomToTop: boolean = false
  ): number {
    let count = 0;
    for (let i = 0; i <= 2 * (dna.length - 1); ++i) {
      let cadena = "";
      for (let j = dna.length - 1; j >= 0; --j) {
        const x = i - (bottomToTop ? dna.length - j : j);
        if (x >= 0 && x < dna.length) {
          cadena += dna[j][x];
        }
      }
      if (cadena.length > 3 && cadena.match(this.validaMutacion)) {
        ++count;
      }
    }
    return count;
  }
}

const mutationService = new MutationService();
export default mutationService;
