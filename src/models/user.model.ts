import mongoose from "../database";

const schema = new mongoose.Schema({
  email:String,
  password: String,
});

const userModel = mongoose.model("user", schema);

export default userModel;
