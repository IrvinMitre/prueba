export interface AdnInterface {
    _id?: string;
    dna: string[];
    dnastr?: string;
    mutacion: boolean;
}