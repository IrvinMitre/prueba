import jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt'

type NormalizePort = (val: number | string) => number | string | boolean

export const normalizePort: NormalizePort = val => {
    const port: number = (typeof val === 'string') ? parseInt(val, 10) : val
    if (isNaN(port)) return val
    else if (port >= 0) return port
    else return false
}

export const makeId = (length: number) => {
    let result = ''
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    const charactersLength = characters.length
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength))
    }
    return result
}

export const generateToken = (rows: any) => {
    // console.log(rows);
    const token = jwt.sign({
        user_id: rows.user_id,
        name: rows.name,
        last_name: rows.last_name,
        email: rows.email
    }, `${process.env.JWT_SECRET}`, {
        expiresIn: 600 * 60 * 24
    })
    return (token)
}

export const hashPassword = async (password: string) => {
    return bcrypt.hashSync(password, Number(`${process.env.SALT_ROUNDS || 10}`));
}

export const verifyPassword = async(password: string, hashedPassword: string) => {
    return bcrypt.compare(password, hashedPassword);
}