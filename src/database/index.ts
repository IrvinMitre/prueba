import { Pool } from 'pg'
import mongoose from 'mongoose';

mongoose.connect(`mongodb://${process.env.MONGODBHOST}:${process.env.MONGODBPORT}/prueba`, {
    auth: {
        user:`${process.env.MONGODBUSER}`,
        password:`${process.env.MONGODBPASSWORD}`
    },
    authSource:"admin",
    useUnifiedTopology: true,
    useNewUrlParser: true
});
export const pool = new Pool()

export default mongoose;
