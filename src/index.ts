
import http from 'http'
import debug from 'debug'
import dotenv from 'dotenv'
import { normalizePort } from './utils'
dotenv.config({ path: `.env.${process.env.NODE_ENV}` })
debug('ts-express:server')
import App from './App'

const port = normalizePort(process.env.PORT || 3001)



const onError = (error: NodeJS.ErrnoException): void => {
    if (error.syscall !== 'listen') throw error
    const bind = (typeof port === 'string') ? 'Pipe ' + port : 'Port ' + port
    switch (error.code) {
      case 'EACCES':
        console.error(`${bind} requires elevated privileges`)
        process.exit(1)
        break
      case 'EADDRINUSE':
        console.error(`${bind} is already in use`)
        process.exit(1)
        break
      default:
        throw error
    }
  }


const onListening = (): void => {
    console.log(` mode is ON`)
  }
  
  App.set('port', port)
  
  
  const server = http.createServer(App)
  server.listen(port)
  server.on('error', onError)
  server.on('listening', onListening)