import { UserInterface } from "../models/user.interface";
import jwt from "jsonwebtoken";

export const createJWTToken = () =>
  jwt.sign({ data: "generic jwt" }, `${process.env.JWT_SECRET}`, {
    algorithm: "HS256",
    expiresIn: process.env.ACCESS_TOKEN_LIFE,
  });

export const verifyJWTToken = (token: string) =>
  jwt.verify(token, process.env.JWT_SECRET || "", { algorithms: ["HS256"] });
