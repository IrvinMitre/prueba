import { Request, Response } from "express";
import mutationService from "../../services/mutation/mutation.service";
import { AdnInterface } from "../../models/dna.interface";
import { ResInterface } from "../../models/res.interface";
class MutationController {
  constructor() {}

  async verifyAdn(req: Request, res: Response) {
    let result;
    let isMutant;
    let errorArray;
    let dna: AdnInterface;

    result = await mutationService.GetDna(req.body.dna);

    if (result.length) {
      if (result[0].mutacion) {
        return res.status(200).send();
      } else {
        return res.status(403).send();
      }
    } else {
      dna = {
        dna: req.body.dna,
        mutacion: false,
      };

      errorArray = await mutationService.dnaValidation(dna.dna);
      if (errorArray !== 5) {
        return res.status(400).send();
      } else {
        isMutant = await mutationService.hasMutation(dna.dna);
        if (isMutant) {
          dna = {
            dna: req.body.dna,
            mutacion: isMutant,
          };
          result = await mutationService.createDna(dna);

          return res.status(200).send();
        } else {
          dna = {
            dna: req.body.dna,
            mutacion: isMutant,
          };
          result = await mutationService.createDna(dna);
          return res.status(403).send();
        }
      }
    }
  }

  async Getstatistics(req: Request, res: Response) {
    let mutant;
    let noMutant;
    let result;
    let ratio;

    result = await mutationService.Getstatistics();
    mutant = result.filter((an: any) => an.mutacion).length;
    noMutant = result.filter((an: any) => an.mutacion === false).length;
    ratio = mutant / (noMutant + mutant);
    let resp: ResInterface = {
      count_mutations: mutant,
      count_no_mutation: noMutant,
      ratio: ratio,
    };
    return res.status(200).send(resp);
  }
}

export default MutationController;
