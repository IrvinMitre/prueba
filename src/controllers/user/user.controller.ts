import { UserInterface } from "./../../models/user.interface";
import { Request, Response } from "express";
import userService from "../../services/user/user.service";
import { createJWTToken } from "../../jwt/auth";

class UserController {
  constructor() {}

  async login(req: Request, res: Response) {
    let result;
    let user;

    result = await userService.login(req.body.email, req.body.password);
    user = createJWTToken();
    res.json({
      user,
    });
  }


}

export default UserController;
